﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week1
{
    class Program
    {
        static void Main(string[] args)
        {
            // There are many different approaches to concatenating the strings from the user's 
            // command line parameters. Here are three different approaches.

            // string.Format method
            string greeting = string.Format("{0} {1}!\n", args[0], args[1]);

            // new, simpler C# 6.0 string interpolation syntax - thanks for pointing this out!
            string greeting1 = $"{args[0]} {args[1]} !\n";

            // classic string concatenation syntax
            string greeting2 = args[0] + " " + args[1] + "!\n"; 

            // write the greeting to the console
            Console.WriteLine(greeting);
        }
    }
}
