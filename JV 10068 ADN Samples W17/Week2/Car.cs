﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week2
{
    class Car : Vehicle
    {
        public void LandMotion()
        {
            Console.Write("Car:Turn");
        }

        public override void Turn()
        {
            Console.Write("Car:Turn!!!!!");
        }

        public override void Maintain()
        {
            Console.Write("Car:Maintain");
        }
    }
}
