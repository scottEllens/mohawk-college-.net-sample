﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week2
{
    public abstract class Vehicle
    {
        private string seats;
        private string dash;
        private string engine;


        public void Accelerate()
        {
            Console.Write("Accelerate:Turn");
        }
        public virtual void Decelerate()
        {
            Console.Write("Decelerate:Turn");
        }
        public virtual void Turn()
        {
            Console.Write("Vehicle:Turn");
        }
        public abstract void Maintain();
    }
}
